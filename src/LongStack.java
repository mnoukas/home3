import java.util.*;

public class LongStack {

   private LinkedList<Long> magasiin;

   public static void main(String[] argum) {
      System.out.println(interpret("2 4"));
   }

   /* Konstruktor */
   LongStack() {
      magasiin = new LinkedList<Long>();
   }

   /* Kloonimismeetod */
   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack kloonTest = new LongStack();
      kloonTest.magasiin = (LinkedList<Long>) magasiin.clone();
      return kloonTest;
   }

   /* Meetod, mis kontrollib, ega magasin tühi ei ole, kui on, siis return true. */
   public boolean stEmpty() {
      if (magasiin.size() == 0) {
         return true;
      }
      return false;
   }

   /* Meetod, mis lükkab uue numbri stacki, numbri saab kaasa parameetrina */
   public void push(long a) {
      magasiin.push(a);
   }

   /* Meetod, mis eemaldab arvu stackist. Lisatud: Tühjuse kontroll */
   public long pop() {
      if (stEmpty()) {
         throw new RuntimeException("Magasin juba tyhi!");
      } else {
         return magasiin.pop();
      }
   }


   /* Operaatorite meetod, mis teeb tehteid kahe stackis oleva (üksteise all/peal) numbritega
      Viide: enos.itcollege.ee/~jpoial/algoritmid/adt.html
      Lisatud Tühjuskontrolli meetod
    */

   public void op(String s) {
      if (magasiin.size() < 2) {
         throw new RuntimeException("Operatsiooni " + s + " teostamiseks ei jätku arve!");
      } else {
         try {
            long number2 = pop();
            long number1 = pop();
            if (s.equals("+")) push(number1 + number2);
            if (s.equals("-")) push(number1 - number2);
            if (s.equals("*")) push(number1 * number2);
            if (s.equals("/")) push(number1 / number2);
         } catch (Exception e) {
            throw new InputMismatchException("Vale operaator " + s);
         }
      }
   }

   /* Meetod, mis vaatab, mis arv on stacki tipus, ilma eemaldamata
      Viide: enos.itcollege.ee/~jpoial/algoritmid/adt.html
    */
   public long tos() {
      if (magasiin.size() != 0) {
         return magasiin.getFirst();
      } else {
         throw new RuntimeException("Magasin tyhi!");
      }
   }

   /* Võrdsuse kontroll, kui juba stacki magasini suurus ei klapi, tagastatakse false.
      Käidakse läbi parameetrina antud objekti elemendid ning võrreldakse neid teise magasiniga,
      kui kasvõi 1 element ei klapi siis võrdsus ei kehti.
    */
   @Override
   public boolean equals(Object o) {
      if (((LongStack) o).magasiin.size() != magasiin.size())
         return false;
      for (int i = 0; i < magasiin.size(); i++) {
         if (((LongStack) o).magasiin.get(i) != magasiin.get(i))
            return false;
      }
      return true;
   }

   /* Teisendus s6neks
      Viide: enos.itcollege.ee/~jpoial/algoritmid/adt.html
    */
   @Override
   public String toString() {
      if (stEmpty()) {
         return "Magasin on tyhi!";
      }
      StringBuffer buff = new StringBuffer();
      for (int i = magasiin.size() - 1; i >= 0; i--) {
         buff.append(String.valueOf(magasiin.get(i)));
      }
      return buff.toString();
   }

   /* Pööratud Poola Kuju
    * Mõtteid: http://www.java2s.com/Code/Java/Collections-Data-Structure/ReversePolishNotation.htm
    */
   public static long interpret(String pol) {

      /* Kontrolli kas on antud parameetrina string kaasa
       * Kui ei, siis viska exception */
      if (pol.isEmpty() || pol == null) {
         throw new RuntimeException("Tyhi avaldis");
      }

      /* Uue LongStack tüüpi objekti loomine
       * Kaasa antud stringi whitespace eemaldamine
       * "Operaatorid" string loomine
       * String-idest koosneva array "elemendid" loomine, kus parameetrina
       * kaasa antud string tükeldatakse */
       LongStack torn = new LongStack();
         pol = pol.trim();
         String operaatorid = "+-*/";
         String[] elemendid = pol.split("\\s+");

      /* For tsükkel, mis käib läbi elemendid array */
         for (int i = 0; i < elemendid.length; i++) {
            /* Kontrolli, kas avaldises on number või operaator
             * Kui operaator siis pushi ta stacki, kui operaator,
             * siis teosta tehe.
             */
            if (!operaatorid.contains(elemendid[i])) {
               try {
                  torn.push(Long.parseLong((elemendid[i])));
               } catch (NumberFormatException e) {
                  throw new RuntimeException("Vigane avaldis " + pol);
               }
            } else {
               // Oleks võimalik kasutada ka Op meetodit.
               try {
                  long esimene = torn.pop();
                  long teine = torn.pop();
                  switch (elemendid[i]) {
                     case "+":
                        torn.push(esimene + teine);
                        break;
                     case "-":
                        torn.push(teine - esimene);
                        break;
                     case "*":
                        torn.push(esimene * teine);
                        break;
                     case "/":
                        torn.push(teine / esimene);
                        break;
                  }
               } catch (Exception e) {
                  throw new RuntimeException("Avaldise " + pol + " teostamine ebaõnnestus!");
               }
            }
         }
         if (torn.magasiin.size() > 1)
            throw new RuntimeException("Liiga palju arve avaldises " + pol);
         return torn.pop();
      }
   }
